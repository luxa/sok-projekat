package rs.ac.uns.ftn.sok.plain.dijalog;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.zest.core.widgets.GraphNode;

public class InformationDialog {

	Display display;
	Shell shell;
	Label label;
	Label label2;
	//private Color color = new Color(Display.getDefault(), new RGB(254, 224, 144));

	public InformationDialog(GraphNode n) {
		super();
		shell = new Shell(Display.getCurrent(), SWT.APPLICATION_MODAL | SWT.SHEET);
		shell.setText("Informations about node");
		shell.setSize(350, 200);

		label = new Label(shell, SWT.WRAP);
		String naziv = n.getText();
		label.setText(" Naziv cvora:  " + naziv + "\n");
		label.pack();

		label2 = new Label(shell, SWT.WRAP | SWT.BORDER);
		label2.setText(
				" Opis: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque lacus arcu, tempus at tortor "
						+ "vel, molestie scelerisque enim. In ac lectus sodales, hendrerit est"
						+ "quis, rutrum urna. Etiam mattis nisl at efficitur ornare. Sed"
						+ "ullamcorper est at elementum tempor. Lorem ipsum dolor sit amet,"
						+ "consectetur adipiscing elit. Aliquam erat volutpat. Ut volutpat ipsum"
						+ "sem, convallis tincidunt leo aliquet vel.");
		label2.pack();

		FillLayout fillLayout = new FillLayout();
		fillLayout.type = SWT.VERTICAL;

		shell.setLayout(fillLayout);
		shell.open();

	}
}
