package rs.ac.uns.ftn.sok.plain;

import java.util.Hashtable;

import org.eclipse.swt.graphics.ImageData;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

import rs.ac.uns.ftn.sok.host.model.IVisualiser;
import rs.ac.uns.ftn.sok.plain.impl.PlainVisualiser;

public class Activator implements BundleActivator {

	public void start(BundleContext context) throws Exception {
		Thread.sleep(100);
		final Hashtable<String, Object> dict = new Hashtable<>();
		dict.put("VISUALISER", new PlainVisualiser());
		dict.put("NAME", "");
		dict.put("DESCRIPTION", "Plain vizualizator");
		ImageData data = new ImageData(getClass().getClassLoader().getResourceAsStream("icons/palin3.png"));
		dict.put("IMAGE", data);
		context.registerService(IVisualiser.class.getName(), new PlainVisualiser(), dict);
	}

	public void stop(BundleContext context) throws Exception {
		System.out.println("Goodbye World!!");
	}

}
