package rs.ac.uns.ftn.sok.plain.impl;

import java.util.HashMap;

import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.MouseWheelListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.zest.core.widgets.Graph;
import org.eclipse.zest.core.widgets.GraphConnection;
import org.eclipse.zest.core.widgets.GraphItem;
import org.eclipse.zest.core.widgets.GraphNode;
import org.eclipse.zest.core.widgets.LayoutFilter;
import org.eclipse.zest.layouts.algorithms.SpringLayoutAlgorithm;

import rs.ac.uns.ftn.sok.host.model.IVisualiser;
import rs.ac.uns.ftn.sok.host.model.graph.SourceEdge;
import rs.ac.uns.ftn.sok.host.model.graph.SourceGraph;
import rs.ac.uns.ftn.sok.host.model.graph.SourceNode;
import rs.ac.uns.ftn.sok.plain.dijalog.InformationDialog;

public class PlainVisualiser implements IVisualiser {

	private SourceGraph graph;
	private int weight;
	Runnable r;
	HashMap<Long, SourceNode> klase;
	HashMap<Long, SourceNode> atributi;
	HashMap<Long, SourceNode> metode;
	HashMap<Long, GraphNode> nacrtaneKlase;
	public Color color2 = new Color(Display.getDefault(), new RGB(240, 240, 240));

	GraphConnection kk;
	GraphConnection ka;
	GraphConnection km;

	@Override
	public Graph drawGraph(SourceGraph graph, Composite panel, int weight) {
		panel.setLayout(new FillLayout());
		Graph g = new Graph(panel, SWT.NONE);

		g.addMouseWheelListener(new MouseWheelListener() {

			@Override
			public void mouseScrolled(MouseEvent e) {
				if (e.stateMask == SWT.CTRL) {
					if (e.count > 0) {
						Point p = g.getZoomManager().getViewport().getViewLocation();
						Point p1 = new Point(e.x, e.y);
						p1.x += p.x;
						p1.y += p.y;
						Point p2 = p1.getCopy();
						double prev_zoom = g.getZoomManager().getZoom();
						double new_zoom = prev_zoom + 0.2;
						if (new_zoom < 4) {
							g.getZoomManager().setZoom(new_zoom);
							p2.scale(new_zoom / prev_zoom);
							Dimension dif = p2.getDifference(p1);
							p.x += dif.width;
							p.y += dif.height;
							g.getZoomManager().getViewport().setViewLocation(p);
						}

					} else {
						Point p = g.getZoomManager().getViewport().getViewLocation();
						Point p1 = new Point(e.x, e.y);
						p1.x += p.x;
						p1.y += p.y;
						Point p2 = p1.getCopy();
						double prev_zoom = g.getZoomManager().getZoom();
						double new_zoom = prev_zoom - 0.2;
						if (new_zoom > 0) {
							g.getZoomManager().setZoom(new_zoom);
							p2.scale(new_zoom / prev_zoom);
							Dimension dif = p2.getDifference(p1);
							p.x += dif.width;
							p.y += dif.height;
							g.getZoomManager().getViewport().setViewLocation(p);
						}
					}
				}
			}
		});

		g.addMouseListener(new MouseListener() {

			@Override
			public void mouseUp(MouseEvent e) {
			}

			@Override
			public void mouseDown(MouseEvent e) {
			}

			@Override
			public void mouseDoubleClick(MouseEvent e) {
				if (g.getSelection().size() >= 1) {
					if (g.getSelection().get(0) instanceof GraphNode) {
						new InformationDialog((GraphNode) g.getSelection().get(0));
					}
				}
			}
		});
		;

		this.graph = graph;

		this.weight = weight;
		klase = new HashMap<Long, SourceNode>();
		atributi = new HashMap<Long, SourceNode>();
		metode = new HashMap<Long, SourceNode>();
		long start = System.currentTimeMillis();
		long end;
		final SpringLayoutAlgorithm springLayoutAlgorithm = new SpringLayoutAlgorithm();
		for (SourceEdge e : graph.getAllEdges()) {
			klase.put(e.getFromNode().getId(), e.getFromNode());
			if (e.getWeight() <= 1) {
				klase.put(e.getToNode().getId(), e.getToNode());
			} else if (e.getWeight() == 2) {
				atributi.put(e.getToNode().getId(), e.getToNode());
			} else if (e.getWeight() == 3) {
				metode.put(e.getToNode().getId(), e.getToNode());
			}
		}
		end = System.currentTimeMillis();
		System.out.println("prva petlja " + (end - start));
		start = end;
		nacrtaneKlase = new HashMap<>();
		for (Long key : klase.keySet()) {
			SourceNode sn = klase.get(key);
			GraphNode n = new GraphNode(g, SWT.NONE, sn.getName());
			n.setBackgroundColor(g.getGraph().getDisplay().getSystemColor(SWT.COLOR_GRAY));

			nacrtaneKlase.put(sn.getId(), n);
			for (SourceEdge e : graph.getAllEdges()) {
				if (weight >= 2 && e.getWeight() == 2 && e.getFromNode().getId() == key) {
					GraphNode a = new GraphNode(g, SWT.NONE, e.getToNode().getName());
					a.setBackgroundColor(g.getGraph().LIGHT_BLUE_CYAN);
					ka = new GraphConnection(g, SWT.NONE, n, a);
					ka.setLineColor(g.getGraph().getDisplay().getSystemColor(SWT.COLOR_DARK_CYAN));
				}
				if (weight >= 3 && e.getWeight() == 3 && e.getFromNode().getId() == key) {
					GraphNode a = new GraphNode(g, SWT.NONE, e.getToNode().getName());
					a.setBackgroundColor(g.getGraph().LIGHT_YELLOW);
					km = new GraphConnection(g, SWT.NONE, n, a);
					km.setLineColor(g.getGraph().getDisplay().getSystemColor(SWT.COLOR_DARK_YELLOW));
				}
			}
		}
		end = System.currentTimeMillis();
		start = end;

		for (SourceEdge e : graph.getAllEdges()) {
			if (e.getWeight() <= 1) {
				kk = new GraphConnection(g, SWT.NONE, nacrtaneKlase.get(e.getFromNode().getId()),
						nacrtaneKlase.get(e.getToNode().getId()));
				kk.setLineColor(g.getGraph().getDisplay().getSystemColor(SWT.COLOR_DARK_GRAY));
			}
		}

		end = System.currentTimeMillis();
		start = end;

		LayoutFilter filter = new LayoutFilter() {
			public boolean isObjectFiltered(GraphItem item) {
				if (item instanceof GraphConnection) {
					GraphConnection connection = (GraphConnection) item;
					Object data = connection.getData();
					return true;
				}
				return false;
			}
		};
		g.addLayoutFilter(filter);

		springLayoutAlgorithm.fitWithinBounds = false;
		springLayoutAlgorithm.setSpringMove(0.25);
		g.setLayoutAlgorithm(springLayoutAlgorithm, true);

		g.pack();

		return g;
	}

}
