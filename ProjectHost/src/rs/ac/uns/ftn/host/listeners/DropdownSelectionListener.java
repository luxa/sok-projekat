package rs.ac.uns.ftn.host.listeners;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.ToolItem;

import rs.ac.uns.ftn.sok.host.MainApplicationWindow;

public class DropdownSelectionListener extends SelectionAdapter {

	private ToolItem dropdown;
	private Shell shell;
	private MainApplicationWindow window;

	private Menu menu;

	public DropdownSelectionListener(ToolItem dropdown, Shell shell, MainApplicationWindow window) {
		this.dropdown = dropdown;
		this.shell = shell;
		this.window = window;
		menu = new Menu(dropdown.getParent().getShell());
	}

	public void add(String item) {
		MenuItem menuItem = new MenuItem(menu, SWT.NONE);
		menuItem.setText(item);
		menuItem.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent event) {
				MenuItem selected = (MenuItem) event.widget;
				final char broj = selected.getText().charAt(selected.getText().length() - 1);
				window.setWeight(Integer.parseInt(String.format("%c", broj)));

			}
		});
	}

	public void widgetSelected(SelectionEvent event) {
		ToolItem item = (ToolItem) event.widget;
		Rectangle rect = item.getBounds();
		Point pt = item.getParent().toDisplay(new Point(rect.x, rect.y));
		menu.setLocation(pt.x, pt.y + rect.height);
		menu.setVisible(true);
	}
}
