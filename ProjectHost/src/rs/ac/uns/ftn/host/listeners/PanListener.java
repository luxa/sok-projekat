package rs.ac.uns.ftn.host.listeners;

import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.MouseMoveListener;
import org.eclipse.zest.core.widgets.Graph;
import org.eclipse.zest.core.widgets.GraphNode;

public class PanListener implements MouseMoveListener, MouseListener {

	private int current_x = 0;
	private int current_y = 0;
	private boolean isPressed = false;
	private Graph graph;

	public PanListener(Graph graph) {
		this.graph = graph;
	}

	@Override
	public void mouseDoubleClick(MouseEvent e) {

	}

	@Override
	public void mouseDown(MouseEvent e) {
		isPressed = true;

	}

	@Override
	public void mouseUp(MouseEvent e) {
		isPressed = false;
	}

	@Override
	public void mouseMove(MouseEvent e) {

		int deltaX = current_x - e.x;
		int deltaY = current_y - e.y;
		current_x = e.x;
		current_y = e.y;

		if (!isPressed)
			return;

		for (Object s : graph.getNodes()) {
			GraphNode a = (GraphNode) s;
			a.setLocation(a.getLocation().x - deltaX, a.getLocation().y - deltaY);
		}
	}

}
