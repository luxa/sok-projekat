package rs.ac.uns.ftn.host.listeners;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

public class FileAboutPackItem extends Shell {

	Display display;
	Shell shell;

	// za about
	GridLayout gl;
	GridData data;
	private Color color2 = new Color(getDisplay(), new RGB(254, 224, 144));

	public FileAboutPackItem(Display display, Shell shell) {
		super(shell);

		this.display = display;
		this.shell = shell;
		makeLayout();

		setText("About");
		setToolTipText("Upoznaj se sa PACK-om");
		setSize(500, 670);

		Image image0 = new Image(getDisplay(), getClass().getClassLoader().getResourceAsStream("icons/about.jpg"));

		data = new GridData();
		data.horizontalAlignment = GridData.FILL;
		data.grabExcessHorizontalSpace = true;
		data.horizontalSpan = 2;
		Label label = new Label(this, SWT.CENTER);
		label.setImage(image0);
		label.setBackground(color2);
		label.setLayoutData(data);

		data = new GridData();
		data.horizontalAlignment = GridData.FILL;

		Button okButton = new Button(this, SWT.PUSH);
		okButton.setText("OK");
		okButton.setLayoutData(data);
		okButton.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				widgetDefaultSelected(e);
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				FileAboutPackItem.this.close();
			}
		});

		open();
	}

	public void makeLayout() {
		gl = new GridLayout(2, true);
		setLayout(gl);

	}

	public void onOkButtonListener(Display display, Shell shell) {

		this.close();

	}

	@Override
	protected void checkSubclass() {
	}

}
