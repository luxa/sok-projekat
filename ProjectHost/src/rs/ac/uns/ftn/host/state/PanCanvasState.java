package rs.ac.uns.ftn.host.state;

import org.eclipse.swt.events.MouseEvent;
import org.eclipse.zest.core.widgets.Graph;
import org.eclipse.zest.core.widgets.GraphNode;

public class PanCanvasState extends CanvasState {

	private int current_x = 0;
	private int current_y = 0;
	private boolean isPressed = false;

	public PanCanvasState(Graph graph) {
		super(graph);
	}

	@Override
	public void mouseDown(MouseEvent e) {
		isPressed = true;
	}

	@Override
	public void mouseUp(MouseEvent e) {
		isPressed = false;
	}

	@Override
	public void mouseMove(MouseEvent e) {

		int deltaX = current_x - e.x;
		int deltaY = current_y - e.y;
		current_x = e.x;
		current_y = e.y;

		if (!isPressed)
			return;

		for (Object s : graph.getNodes()) {
			GraphNode a = (GraphNode) s;
			a.setLocation(a.getLocation().x - deltaX, a.getLocation().y - deltaY);
		}

	}

}
