package rs.ac.uns.ftn.host.state;

import org.eclipse.swt.events.MouseEvent;
import org.eclipse.zest.core.widgets.Graph;

public abstract class CanvasState {

	protected Graph graph;

	public CanvasState(Graph graph) {
		this.graph = graph;
	}

	public abstract void mouseDown(MouseEvent e);

	public abstract void mouseUp(MouseEvent e);

	public abstract void mouseMove(MouseEvent e);

}
