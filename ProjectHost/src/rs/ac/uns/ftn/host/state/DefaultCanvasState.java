package rs.ac.uns.ftn.host.state;

import org.eclipse.swt.events.MouseEvent;
import org.eclipse.zest.core.widgets.Graph;

public class DefaultCanvasState extends CanvasState {

	public DefaultCanvasState(Graph graph) {
		super(graph);
	}

	@Override
	public void mouseDown(MouseEvent e) {
	}

	@Override
	public void mouseUp(MouseEvent e) {
	}

	@Override
	public void mouseMove(MouseEvent e) {
	}

}
