package rs.ac.uns.ftn.host.treeview;

import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;

public class TreeContentProvider implements ITreeContentProvider {

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

	@Override
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {

	}

	/**
	 * Vraca samo root elemente za iscrtavanje u stablu. (to je samo tezina1)
	 * 
	 * @param arg0
	 *            the input data
	 * @return Object[]
	 */
	@Override
	public Object[] getElements(Object inputElement) {

		return ((TreeNode) inputElement).child.toArray();
	}

	@Override
	public Object[] getChildren(Object parentElement) {

		return getElements(parentElement);
	}

	@Override
	public Object getParent(Object element) {

		if (element == null) {
			return null;
		}

		return ((TreeNode) element).parent;
	}

	@Override
	public boolean hasChildren(Object element) {

		return ((TreeNode) element).child.size() > 0;
	}

}
