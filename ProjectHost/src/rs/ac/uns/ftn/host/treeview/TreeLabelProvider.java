package rs.ac.uns.ftn.host.treeview;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.swt.graphics.Image;

public class TreeLabelProvider implements ILabelProvider {
	private List<ILabelProviderListener> listeners;

	public TreeLabelProvider() {
		super();
		listeners = new ArrayList<ILabelProviderListener>();

		// Create the images
		klasa = new Image(null, getClass().getClassLoader().getResourceAsStream("icons/klasa.png"));
		atribut = new Image(null, getClass().getClassLoader().getResourceAsStream("icons/atribut.png"));
		metoda = new Image(null, getClass().getClassLoader().getResourceAsStream("icons/metoda.png"));
	}

	// Images for tree nodes
	private Image klasa;
	private Image metoda;
	private Image atribut;

	@Override
	public void addListener(ILabelProviderListener listener) {
		listeners.add(listener);

	}

	@Override
	public void dispose() {

	}

	@Override
	public boolean isLabelProperty(Object element, String property) {
		return false;
	}

	@Override
	public void removeListener(ILabelProviderListener listener) {
		listeners.remove(listener);

	}

	@Override
	public Image getImage(Object element) {
		if (element instanceof TreeNode) {
			TreeNode node = (TreeNode) element;
			if (node.type == 1) {
				return klasa;
			} else if (node.type == 3) {
				return metoda;
			} else if (node.type == 2) {
				return atribut;
			}
		}
		return null;
	}

	@Override
	public String getText(Object element) {
		if (element instanceof TreeNode) {
			if (((TreeNode) element).name == null) {
				return "NEMA IMENA!!!";
			} else {
				return ((TreeNode) element).name;
			}
		}
		return null;
	}

}
