package rs.ac.uns.ftn.host.treeview;

import java.util.ArrayList;
import java.util.List;

public class TreeNode {

	public TreeNode parent;
	public List<TreeNode> child = new ArrayList<>();
	/*
	 * 0 - root 1 - klasa 2 - atribut 3 - metoda
	 */
	public int type;
	public String name;

	public TreeNode(int type, TreeNode parent, String name) {
		this.parent = parent;
		this.type = type;
		this.name = name;
	}

	@Override
	public String toString() {

		return "TreeNode: " + name;
	}

}
