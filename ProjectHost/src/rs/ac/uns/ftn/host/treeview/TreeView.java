package rs.ac.uns.ftn.host.treeview;

import java.util.HashMap;

import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.widgets.Composite;

import rs.ac.uns.ftn.sok.host.model.graph.SourceEdge;
import rs.ac.uns.ftn.sok.host.model.graph.SourceGraph;

public class TreeView {

	SourceGraph g;

	public TreeView(SourceGraph g, Composite parent) {
		if (g != null) {
			this.g = g;

			/*
			 * Composite composite = new Composite(parent, SWT.NONE);
			 * composite.setLayout(new GridLayout(1, false));
			 * 
			 * getTree().setLayoutData(new GridData(GridData.FILL_BOTH));
			 */
			final TreeViewer v = new TreeViewer(parent);
			v.setLabelProvider(new TreeLabelProvider());
			v.setContentProvider(new TreeContentProvider());
			v.setInput(createModel());
			parent.pack();

		}
	}

	private TreeNode createModel() {
		TreeNode root = new TreeNode(0, null, "");
		HashMap<Long, TreeNode> klase = new HashMap<>();

		for (SourceEdge e : g.getAllEdges()) {
			TreeNode from = new TreeNode(1, root, e.getFromNode().getName());
			klase.put(e.getFromNode().getId(), from);

			if (e.getWeight() <= 1) {
				TreeNode toNode = new TreeNode(1, root, e.getToNode().getName());
				klase.put(e.getFromNode().getId(), toNode);
			}
		}

		for (Long key : klase.keySet()) {
			TreeNode sn = klase.get(key);
			root.child.add(sn);
			for (SourceEdge e : g.getAllEdges()) {
				if (e.getWeight() == 2 && e.getFromNode().getId() == key) {
					TreeNode a = new TreeNode(2, klase.get(e.getFromNode().getId()), e.getToNode().getName());
					sn.child.add(a);
				}
				if (e.getWeight() == 3 && e.getFromNode().getId() == key) {
					TreeNode a = new TreeNode(3, klase.get(e.getFromNode().getId()), e.getToNode().getName());
					sn.child.add(a);
				}
			}
		}
		return root;
	}
}
