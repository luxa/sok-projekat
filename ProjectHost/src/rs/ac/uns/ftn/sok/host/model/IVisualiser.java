package rs.ac.uns.ftn.sok.host.model;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.zest.core.widgets.Graph;

import rs.ac.uns.ftn.sok.host.model.graph.SourceGraph;

public interface IVisualiser {

	Graph drawGraph(SourceGraph graph, Composite panel, int weight);

}
