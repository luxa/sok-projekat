package rs.ac.uns.ftn.sok.host;

import java.util.ArrayList;
import java.util.HashMap;

import org.eclipse.draw2d.GroupBoxBorder;
import org.eclipse.draw2d.LightweightSystem;
import org.eclipse.draw2d.parts.ScrollableThumbnail;
import org.eclipse.draw2d.parts.Thumbnail;
import org.eclipse.jface.window.ApplicationWindow;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.ControlListener;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.eclipse.wb.swt.SWTResourceManager;
import org.eclipse.zest.core.widgets.Graph;
import org.eclipse.zest.core.widgets.GraphNode;
import org.osgi.framework.BundleContext;

import rs.ac.uns.ftn.host.listeners.DropdownSelectionListener;
import rs.ac.uns.ftn.host.listeners.FileAboutPackItem;
import rs.ac.uns.ftn.host.listeners.HelpGetHelpItemListener;
import rs.ac.uns.ftn.host.state.CanvasState;
import rs.ac.uns.ftn.host.state.DefaultCanvasState;
import rs.ac.uns.ftn.host.state.PanCanvasState;
import rs.ac.uns.ftn.host.treeview.TreeView;
import rs.ac.uns.ftn.sok.host.bundles.SourceBundle;
import rs.ac.uns.ftn.sok.host.bundles.VisualiserBundle;
import rs.ac.uns.ftn.sok.host.model.IVisualiser;
import rs.ac.uns.ftn.sok.host.model.graph.SourceGraph;
import rs.ac.uns.ftn.sok.host.proxy.VisualiserProxy;
import rs.ac.uns.ftn.sok.host.servicetrackers.SourceTracker;
import rs.ac.uns.ftn.sok.host.servicetrackers.VisualiserTracker;

public class MainApplicationWindow extends ApplicationWindow {

	private Canvas canvas;
	private Composite topPanel;
	private Composite mainPanel;
	private Composite birdViewComposite;
	private Canvas realBirdView;
	private Composite treeViewComposite;
	private Composite realTreeView;
	private CanvasState currentCanvasState;
	private SourceTracker sourceTracker;
	private VisualiserTracker visualiserTracker;
	Shell helpWindow;
	private Color color = new Color(Display.getDefault(), new RGB(254, 224, 144));
	private Color color2 = new Color(Display.getDefault(), new RGB(240, 240, 240));
	private Color color3 = new Color(Display.getDefault(), new RGB(255, 255, 255));
	private GridLayout gridLayout;
	private GridData data;
	private ToolBar sourceToolbar;
	private ToolBar visualiserToolbar;
	private ToolBar funcToolbar;
	private Menu menuBar, helpMenu;
	private MenuItem helpMenuHeader, aboutMenuHeader;
	private MenuItem helpGetHelpItem, aboutPackItem;
	private ArrayList<SourceBundle> sourceBundles = new ArrayList<SourceBundle>();
	private ArrayList<VisualiserBundle> visualiserBundles = new ArrayList<VisualiserBundle>();
	private HashMap<SourceBundle, ToolItem> sourceButtons = new HashMap<>();
	private HashMap<VisualiserBundle, ToolItem> visualiserButtons = new HashMap<>();
	private SourceGraph sourceGraph;
	private Graph graph;
	private int weight = 3;
	private IVisualiser visualiser;
	private Menu aboutMenu;
	private Text searchText;
	private Shell shell;
	private BundleContext context;

	public MainApplicationWindow(BundleContext context) {
		super(null);
		this.context = context;
		this.visualiser = new VisualiserProxy();
	}

	protected Control createContents(Composite parent) {
		this.shell = getShell();

		parent.setLayout(new GridLayout(1, false));
		shell.setImage(
				new Image(getShell().getDisplay(), getClass().getClassLoader().getResourceAsStream("icons/icon.png")));
		shell.setText("PACK ExPreSsiVeNess");
		shell.setLocation(0, 0);
		shell.setBackground(color);

		makeLayout();
		makePanels(parent);
		createSourceToolbar();
		createFuncToolbar();
		creatSearchField();
		createVisualizerToolbar();
		createMenuBar(parent.getDisplay());

		sourceTracker = new SourceTracker(context, this);
		sourceTracker.open();
		visualiserTracker = new VisualiserTracker(context, this);
		visualiserTracker.open();

		return parent;
	}

	public boolean isDisposed() {
		return shell.isDisposed();
	}

	public void setSourceGraph(SourceGraph sourceGraph) {
		this.sourceGraph = sourceGraph;
		fireGraphChanged();
		makeTreeView();
	}

	private void fireGraphChanged() {
		if (graph != null) {
			graph.dispose();
		}
		Display.getCurrent().asyncExec(() -> {
			graph = MainApplicationWindow.this.visualiser.drawGraph(sourceGraph, canvas, weight);
			if (graph != null) {
				pan();
				birdView();
			}
		});

	}

	private synchronized void setVisualiser(IVisualiser visualiser) {
		this.visualiser = new VisualiserProxy(visualiser);
		fireGraphChanged();
	}

	Thumbnail s;

	private Thumbnail createGraphThumbnail() {

		s = new ScrollableThumbnail(graph.getViewport());
		s.setBorder(new GroupBoxBorder("Graph overview"));
		s.setFocusTraversable(true);
		s.setSource(graph.getViewport());

		if (graph != null) {
			graph.addControlListener(new ControlListener() {

				@Override
				public void controlResized(ControlEvent e) {
					s.repaint();
				}

				@Override
				public void controlMoved(ControlEvent e) {
					s.repaint();
				}
			});
		}

		return s;
	}

	public void birdView() {
		if (realBirdView != null) {
			realBirdView.dispose();
			realBirdView = new Canvas(birdViewComposite, SWT.NONE);
			realBirdView.pack();
			birdViewComposite.layout();
		}
		LightweightSystem s = new LightweightSystem(realBirdView);
		s.setContents(createGraphThumbnail());
	}

	public void pan() {

		graph.addMouseListener(new MouseListener() {

			@Override
			public void mouseUp(MouseEvent e) {
				if (currentCanvasState != null)
					currentCanvasState.mouseUp(e);
			}

			@Override
			public void mouseDown(MouseEvent e) {
				if (currentCanvasState != null)
					currentCanvasState.mouseDown(e);
			}

			@Override
			public void mouseDoubleClick(MouseEvent e) {
			}
		});
		graph.addMouseMoveListener((e) -> {
			if (currentCanvasState != null) {
				currentCanvasState.mouseMove(e);
			}
		});
	}

	public void makePanels(Composite parent) {

		data = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
		data.heightHint = 50;
		topPanel = new Composite(parent, SWT.NONE);
		topPanel.setLayout(new GridLayout(4, true));
		topPanel.setLayoutData(data);
		topPanel.setBackground(color2);
		topPanel.pack();

		data = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		mainPanel = new Composite(parent, SWT.NONE);
		mainPanel.setLayout(new GridLayout(2, false));
		mainPanel.setLayoutData(data);
		mainPanel.setBackground(color);
		mainPanel.pack();

		data = new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1);
		data.heightHint = 200;
		data.widthHint = 300;
		birdViewComposite = new Composite(mainPanel, SWT.NONE);
		birdViewComposite.setLayout(new FillLayout());
		birdViewComposite.setBackground(SWTResourceManager.getColor(255, 255, 255));
		birdViewComposite.setLayoutData(data);
		birdViewComposite.pack();

		realBirdView = new Canvas(birdViewComposite, SWT.NONE);
		realBirdView.setBackground(SWTResourceManager.getColor(255, 255, 255));
		realBirdView.pack();
		
		data = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 2);
		canvas = new Canvas(mainPanel, SWT.NONE);
		canvas.setLayoutData(data);
		canvas.setBackground(color3);
		canvas.pack();

		data = new GridData(SWT.FILL, SWT.FILL, false, true, 1, 1);
		data.widthHint = 300;
		treeViewComposite = new Composite(mainPanel, SWT.NONE);
		treeViewComposite.setLayoutData(data);
		treeViewComposite.setBackground(color3);
		treeViewComposite.setLayout(new FillLayout());
		treeViewComposite.pack();

	}

	public void makeLayout() {

		gridLayout = new GridLayout(1, false);
		shell.setLayout(gridLayout);

	}

	protected void createMenuBar(Display display) {

		menuBar = new Menu(shell, SWT.BAR);

		helpMenuHeader = new MenuItem(menuBar, SWT.CASCADE);
		helpMenuHeader.setText("&Help");

		helpMenu = new Menu(shell, SWT.DROP_DOWN);
		helpMenuHeader.setMenu(helpMenu);

		helpGetHelpItem = new MenuItem(helpMenu, SWT.PUSH);
		helpGetHelpItem.setText("&Get Help");

		aboutMenuHeader = new MenuItem(menuBar, SWT.CASCADE);
		aboutMenuHeader.setText("&About");

		aboutMenu = new Menu(shell, SWT.DROP_DOWN);
		aboutMenuHeader.setMenu(aboutMenu);

		aboutPackItem = new MenuItem(aboutMenu, SWT.PUSH);
		aboutPackItem.setText("&About PACK");

		helpGetHelpItem.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				new HelpGetHelpItemListener(display, shell);
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				widgetSelected(e);
			}
		});
		aboutPackItem.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				new FileAboutPackItem(display, shell);
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				widgetSelected(e);
			}
		});

		shell.setMenuBar(menuBar);
	}

	private Image getImageByResourceName(String resourceName) {
		return new Image(Display.getDefault(), getClass().getClassLoader().getResourceAsStream(resourceName));
	}

	public void createFuncToolbar() {

		Image image4 = getImageByResourceName("icons/zoomIn3.png");
		Image image5 = getImageByResourceName("icons/zoomOut3.png");
		Image image7 = getImageByResourceName("icons/filter3.png");
		Image image8 = getImageByResourceName("icons/cursor-default.png");
		Image image9 = getImageByResourceName("icons/pan.png");

		data = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		funcToolbar = new ToolBar(topPanel, SWT.HORIZONTAL | SWT.WRAP);
		funcToolbar.setBackground(color2);

		ToolItem defaultItem = new ToolItem(funcToolbar, SWT.PUSH);
		defaultItem.setToolTipText("Default state...");
		defaultItem.setImage(image8);
		defaultItem.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				currentCanvasState = new DefaultCanvasState(graph);
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				widgetSelected(e);
			}
		});

		ToolItem panCanvasItem = new ToolItem(funcToolbar, SWT.PUSH);
		panCanvasItem.setToolTipText("Move canvas");
		panCanvasItem.setImage(image9);
		panCanvasItem.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				currentCanvasState = new PanCanvasState(graph);
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				widgetSelected(e);
			}
		});

		ToolItem filterItem = new ToolItem(funcToolbar, SWT.DROP_DOWN);
		filterItem.setToolTipText("Odaberi tezinu prikaza");
		filterItem.setImage(image7);

		DropdownSelectionListener listenerOne = new DropdownSelectionListener(filterItem, shell, this);
		listenerOne.add("Te�ina 1");
		listenerOne.add("Te�ina 2");
		listenerOne.add("Te�ina 3");
		filterItem.addSelectionListener(listenerOne);

		ToolItem zoomInItem = new ToolItem(funcToolbar, SWT.PUSH);
		zoomInItem.setToolTipText("Zoom +");
		zoomInItem.setImage(image4);
		zoomInItem.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				widgetDefaultSelected(e);

			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				if (graph != null) {
					double zoom = graph.getZoomManager().getZoom() + 0.2;
					if (zoom < graph.getZoomManager().getMaxZoom()) {
						graph.getZoomManager().setZoom(zoom);
					}
				}
			}
		});

		ToolItem zoomOutItem = new ToolItem(funcToolbar, SWT.PUSH);
		zoomOutItem.setToolTipText("Zoom -");
		zoomOutItem.setImage(image5);
		zoomOutItem.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				widgetDefaultSelected(e);

			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
				if (graph != null) {
					double zoom = graph.getZoomManager().getZoom() - 0.2;
					if (zoom > graph.getZoomManager().getMinZoom()) {
						graph.getZoomManager().setZoom(zoom);
					}
				}
			}
		});

		funcToolbar.pack();

	}

	public void setWeight(int weight) {
		this.weight = weight;
		fireGraphChanged();
	}

	public void createSourceToolbar() {

		data = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		sourceToolbar = new ToolBar(topPanel, SWT.HORIZONTAL | SWT.WRAP);
		sourceToolbar.setBackground(color2);

		sourceToolbar.pack();
	}

	public void creatSearchField() {

		data = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		searchText = new Text(topPanel, SWT.BORDER);
		searchText.addModifyListener(new ModifyListener() {

			@Override
			public void modifyText(ModifyEvent e) {
				// TODO Auto-generated method stub
				if (graph != null) {
					for (GraphNode n : (Iterable<GraphNode>) graph.getNodes()) {
						if (n.getText().toLowerCase().contains(searchText.getText().toLowerCase())) {
							n.setVisible(true);
						} else {
							n.setVisible(false);
						}
					}

				}
			}
		});
		searchText.pack();
	}

	public void createVisualizerToolbar() {

		data = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		visualiserToolbar = new ToolBar(topPanel, SWT.HORIZONTAL | SWT.WRAP);
		visualiserToolbar.setBackground(color2);

		ToolItem itemSeparator = new ToolItem(funcToolbar, SWT.SEPARATOR);
		itemSeparator.setWidth(40);

		visualiserToolbar.pack();
	}

	public void addVisualiser(VisualiserBundle bundle) {
		shell.getDisplay().syncExec(() -> {
			final ToolItem visualiserToolItem = new ToolItem(visualiserToolbar, SWT.RADIO);
			visualiserToolItem.setText(bundle.getName());
			visualiserToolItem.setImage(new Image(shell.getDisplay(), bundle.getImage()));
			visualiserToolItem.setToolTipText(bundle.getTooltip());
			visualiserToolItem.addSelectionListener(new SelectionListener() {
				private VisualiserBundle visualiserBundle = bundle;

				@Override
				public void widgetSelected(SelectionEvent e) {
					boolean isSelected = ((ToolItem) e.getSource()).getSelection();
					if (isSelected) {
						setVisualiser(visualiserBundle.getVisualiser());
					}
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
					widgetSelected(e);
				}
			});
			visualiserButtons.put(bundle, visualiserToolItem);
			visualiserBundles.add(bundle);
			setVisualiser(bundle.getVisualiser());
			visualiserToolbar.pack();
			topPanel.pack();
			visualiserToolbar.layout();
			topPanel.layout();
		});
	}

	public void removeVisualiser(VisualiserBundle bundle) {
		synchronized (visualiserToolbar) {
			shell.getDisplay().syncExec(() -> {
				visualiserButtons.get(bundle).dispose();
				visualiserBundles.remove(bundle);
				visualiserButtons.remove(bundle);
				if (visualiserBundles.size() > 0) {
					setVisualiser(visualiserBundles.get(0).getVisualiser());
				} else {
					setVisualiser(null);
				}

			});
		}
	}

	public void addSource(SourceBundle bundle) {
		shell.getDisplay().syncExec(() -> {
			final ToolItem sourceToolItem = new ToolItem(sourceToolbar, SWT.RADIO);
			sourceToolItem.setText(bundle.getName());
			sourceToolItem.setImage(new Image(shell.getDisplay(), bundle.getImage()));
			sourceToolItem.setToolTipText(bundle.getTooltip());
			sourceToolItem.addSelectionListener(new SelectionListener() {
				private SourceBundle sourceBundle = bundle;

				@Override
				public void widgetSelected(SelectionEvent e) {
					boolean isSelected = ((ToolItem) e.getSource()).getSelection();
					if (isSelected) {
						sourceGraph = sourceBundle.getSource().getSourceGraph(MainApplicationWindow.this);
						makeTreeView();
						if (sourceGraph != null) {
							fireGraphChanged();
						}

					}
				}

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
					widgetSelected(e);
				}
			});
			sourceButtons.put(bundle, sourceToolItem);
			sourceBundles.add(bundle);
			sourceToolbar.pack();
			topPanel.pack();
			sourceToolbar.layout();
			topPanel.layout();
		});
	}

	private void makeTreeView() {
		if (sourceGraph != null) {
			if (realTreeView != null) {
				realTreeView.dispose();
			}
			realTreeView = new Composite(treeViewComposite, SWT.FILL);
			realTreeView.setLayout(new FillLayout());
			new TreeView(sourceGraph, realTreeView);
			realTreeView.pack();
			treeViewComposite.layout();
		}
	}

	public void removeSource(SourceBundle bundle) {
		synchronized (sourceToolbar) {
			shell.getDisplay().syncExec(() -> {
				sourceButtons.get(bundle).dispose();
			});
		}
	}

}
