package rs.ac.uns.ftn.sok.host.model.graph;

import java.util.concurrent.atomic.AtomicLong;

public abstract class SourceEdge {

	private static AtomicLong atomicLong = new AtomicLong();
	protected long id;
	protected SourceNode fromNode;
	protected SourceNode toNode;
	protected int weight;

	public SourceEdge() {
		this.id = atomicLong.incrementAndGet();
	}

	public SourceEdge(SourceNode fromNode, SourceNode toNode, int weight) {
		super();
		this.id = atomicLong.incrementAndGet();
		this.fromNode = fromNode;
		this.toNode = toNode;
		this.weight = weight;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null || !(obj instanceof SourceEdge)) {
			return false;
		} else {
			SourceEdge s = (SourceEdge) obj;
			return s.getFromNode().equals(this.getFromNode()) && s.getToNode().equals(this.getToNode())
					&& s.getId() == this.getId();
		}
	}

	@Override
	public String toString() {
		return "SourceEdge [id=" + id + ", fromNode=" + fromNode + ", toNode=" + toNode + ", weight=" + weight + "]"
				+ "\n";
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public SourceNode getFromNode() {
		return fromNode;
	}

	public SourceNode getToNode() {
		return toNode;
	}

	public int getWeight() {
		return weight;
	}

	public void setFromNode(SourceNode fromNode) {
		this.fromNode = fromNode;
	}

	public void setToNode(SourceNode toNode) {
		this.toNode = toNode;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}
}
