package rs.ac.uns.ftn.sok.host.bundles;

import org.eclipse.swt.graphics.ImageData;
import org.osgi.framework.ServiceReference;

import rs.ac.uns.ftn.sok.host.model.IVisualiser;

public class VisualiserBundle {

	private IVisualiser visualiser;
	private String name;
	private String tooltip;
	private ImageData image;

	public VisualiserBundle() {
	}

	public VisualiserBundle(ServiceReference ref) {
		visualiser = (IVisualiser) ref.getProperty("VISUALISER");
		name = (String) ref.getProperty("NAME");
		tooltip = (String) ref.getProperty("DESCRIPTION");
		image = (ImageData) ref.getProperty("IMAGE");
	}

	public IVisualiser getVisualiser() {
		return visualiser;
	}

	public void setVisualiser(IVisualiser visualiser) {
		this.visualiser = visualiser;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTooltip() {
		return tooltip;
	}

	public void setTooltip(String tooltip) {
		this.tooltip = tooltip;
	}

	public ImageData getImage() {
		return image;
	}

	public void setImage(ImageData image) {
		this.image = image;
	}

}
