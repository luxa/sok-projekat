package rs.ac.uns.ftn.sok.host.model.graph;

import java.util.concurrent.atomic.AtomicLong;

public abstract class SourceNode {

	private static AtomicLong atomicLong = new AtomicLong();

	protected String name;
	protected final long id;


	public SourceNode() {
		super();
		id = atomicLong.incrementAndGet();
		
	}

	public SourceNode(String name) {
		super();
		this.id = atomicLong.incrementAndGet();
		this.name = name;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null || !(obj instanceof SourceNode)) {
			return false;
		} else {
			SourceNode s = (SourceNode) obj;
			return s.getId() == this.getId();
		}
	}

	public long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return "SourceNode [name=" + name + ", id=" + id + "]"+"\n";
	}

	public void setName(String name) {
		this.name = name;
	}
}
