package rs.ac.uns.ftn.sok.host.servicetrackers;

import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.ServiceTracker;

import rs.ac.uns.ftn.sok.host.MainApplicationWindow;
import rs.ac.uns.ftn.sok.host.bundles.VisualiserBundle;
import rs.ac.uns.ftn.sok.host.model.IVisualiser;

public class VisualiserTracker extends ServiceTracker<VisualiserBundle, VisualiserBundle> {

	private static enum VisualiserState {
		ADDED, MODIFIED, REMOVED
	};

	private BundleContext context;
	private MainApplicationWindow window;

	public VisualiserTracker(BundleContext context, MainApplicationWindow window) {
		super(context, IVisualiser.class.getName(), null);
		this.context = context;
		this.window = window;
	}

	public VisualiserBundle addingService(ServiceReference ref) {
		VisualiserBundle visualiser = new VisualiserBundle(ref);
		processShapeOnEventThread(VisualiserState.ADDED, ref, visualiser);
		return visualiser;
	}

	public void modifiedService(ServiceReference ref, VisualiserBundle svc) {
		processShapeOnEventThread(VisualiserState.MODIFIED, ref, svc);
	}

	public void removedService(ServiceReference ref, VisualiserBundle svc) {
		processShapeOnEventThread(VisualiserState.REMOVED, ref, svc);
	}

	private synchronized void processShapeOnEventThread(VisualiserState action, ServiceReference ref, VisualiserBundle visualiser) {
		processVisualiser(action, ref, visualiser);
	}

	private void processVisualiser(VisualiserState action, ServiceReference ref, VisualiserBundle visualiser) {

		switch (action) {
		case MODIFIED:
			window.removeVisualiser(visualiser);
		case ADDED:
			window.addVisualiser(visualiser);
			break;
		case REMOVED:
			window.removeVisualiser(visualiser);
			break;
		}
	}

}
