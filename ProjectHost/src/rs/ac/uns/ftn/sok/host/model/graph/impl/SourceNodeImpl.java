package rs.ac.uns.ftn.sok.host.model.graph.impl;

import rs.ac.uns.ftn.sok.host.model.graph.SourceNode;

public class SourceNodeImpl extends SourceNode {

	public SourceNodeImpl() {
	}

	public SourceNodeImpl(String name) {
		super(name);
	}

}
