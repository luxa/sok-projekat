package rs.ac.uns.ftn.sok.host.servicetrackers;

import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.ServiceTracker;

import rs.ac.uns.ftn.sok.host.MainApplicationWindow;
import rs.ac.uns.ftn.sok.host.bundles.SourceBundle;
import rs.ac.uns.ftn.sok.host.model.ISource;

public class SourceTracker extends ServiceTracker<SourceBundle, SourceBundle> {

	private BundleContext context;
	
	private static enum SourceState {
		ADDED, MODIFIED, REMOVED
	};

	private MainApplicationWindow window;

	public SourceTracker(BundleContext context, MainApplicationWindow window) {
		super(context, ISource.class.getName(), null);
		this.context = context;
		this.window = window;
	}

	public SourceBundle addingService(ServiceReference ref) {
		SourceBundle source = new SourceBundle(ref);
		processSourceOnEventThread(SourceState.ADDED, ref, source);
		return source;
	}

	public void modifiedService(ServiceReference ref, SourceBundle svc) {
		processSourceOnEventThread(SourceState.MODIFIED, ref, svc);
	}

	public void removedService(ServiceReference ref, SourceBundle svc) {
		processSourceOnEventThread(SourceState.REMOVED, ref, svc);
	}

	private void processSourceOnEventThread(SourceState action, ServiceReference ref, SourceBundle source) {
		processSource(action, ref, source);
	}

	private void processSource(SourceState action, ServiceReference ref, SourceBundle source) {
		switch (action) {
		case MODIFIED:
			window.removeSource(source);
		case ADDED:
			window.addSource(source);
			break;
		case REMOVED:
			window.removeSource(source);
			break;
		}
	}

}
