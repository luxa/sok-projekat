package rs.ac.uns.ftn.sok.host.bundles;

import org.eclipse.swt.graphics.ImageData;
import org.osgi.framework.ServiceReference;

import rs.ac.uns.ftn.sok.host.model.ISource;

public class SourceBundle {
	private ImageData image;
	private String name;
	private String tooltip;
	private ISource source;

	public SourceBundle(ServiceReference<ISource> ref) {
		this.image = (ImageData) ref.getProperty("IMAGE");
		this.tooltip = (String) ref.getProperty("TOOLTIP");
		this.name = (String) ref.getProperty("NAME");
		this.source = (ISource) ref.getProperty("SOURCE");
	}

	public ImageData getImage() {
		return image;
	}

	public void setImage(ImageData image) {
		this.image = image;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTooltip() {
		return tooltip;
	}

	public void setTooltip(String tooltip) {
		this.tooltip = tooltip;
	}

	public ISource getSource() {
		return source;
	}

	public void setSource(ISource source) {
		this.source = source;
	}

}
