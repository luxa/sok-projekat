package rs.ac.uns.ftn.sok.host.proxy;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.zest.core.widgets.Graph;

import rs.ac.uns.ftn.sok.host.model.IVisualiser;
import rs.ac.uns.ftn.sok.host.model.graph.SourceGraph;

public class VisualiserProxy implements IVisualiser {

	private IVisualiser visualiser;

	@Override
	public Graph drawGraph(SourceGraph graph, Composite panel, int weight) {
		Graph g = null;
		if (graph != null) {
			synchronized (this) {
				if (visualiser != null) {
					g = visualiser.drawGraph(graph, panel, weight);
				}
				panel.layout();
			}
		}
		return g;
	}

	public VisualiserProxy() {
	}

	public VisualiserProxy(IVisualiser visualiser) {
		super();
		this.visualiser = visualiser;
	}

	public IVisualiser getVisualiser() {
		return visualiser;
	}

	public void setVisualiser(IVisualiser visualiser) {
		this.visualiser = visualiser;
	}

}
