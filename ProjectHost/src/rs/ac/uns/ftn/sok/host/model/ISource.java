package rs.ac.uns.ftn.sok.host.model;

import rs.ac.uns.ftn.sok.host.MainApplicationWindow;
import rs.ac.uns.ftn.sok.host.model.graph.SourceGraph;

public interface ISource {
	
	SourceGraph getSourceGraph(MainApplicationWindow window);

}
