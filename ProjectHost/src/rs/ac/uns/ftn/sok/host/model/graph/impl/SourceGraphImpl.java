package rs.ac.uns.ftn.sok.host.model.graph.impl;

import rs.ac.uns.ftn.sok.host.model.graph.SourceEdge;
import rs.ac.uns.ftn.sok.host.model.graph.SourceGraph;
import rs.ac.uns.ftn.sok.host.model.graph.SourceNode;

public class SourceGraphImpl extends SourceGraph {

	public SourceGraphImpl() {
		super();
	}

	@Override
	public void addEdge(SourceNode fromNode, SourceNode toNode, int weight) {
		SourceEdge edge = new SourceEdgeImpl(fromNode, toNode, weight);
		allEdges.add(edge);

	}

	@Override
	public void addEdge(SourceEdge edge) {
		allEdges.add(edge);
	}

	@Override
	public void removeEdge(SourceEdge edge) {
		allEdges.remove(edge);
	}

	@Override
	public void addNode(SourceNode node) {
		allNodes.add(node);

	}

	@Override
	public void removeNode(SourceNode node) {
		allNodes.remove(node);

	}

}
