package rs.ac.uns.ftn.sok.host;

import java.awt.EventQueue;

import org.eclipse.swt.widgets.Display;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

public class Activator implements BundleActivator, Runnable {

	private BundleContext context = null;
	private MainApplicationWindow frame = null;

	public void start(BundleContext context) throws Exception {
		this.context = context;
		EventQueue.invokeLater(this);
	}

	public void stop(BundleContext context) throws Exception {
		System.out.println("Goodbye World!!");
		Display.getDefault().asyncExec(() -> frame.getShell().getDisplay().dispose());
	}

	@Override
	public void run() {

		MainApplicationWindow mainApplicationWindow = new MainApplicationWindow(context);
		mainApplicationWindow.setBlockOnOpen(true);
		mainApplicationWindow.open();

	}

}
