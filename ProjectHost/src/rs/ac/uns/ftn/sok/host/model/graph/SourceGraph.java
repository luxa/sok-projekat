package rs.ac.uns.ftn.sok.host.model.graph;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.zest.layouts.algorithms.TreeLayoutAlgorithm;

public abstract class SourceGraph {

	protected List<SourceEdge> allEdges;

	protected List<SourceNode> allNodes;

	public SourceGraph() {
		allEdges = new ArrayList<SourceEdge>();
		allNodes = new ArrayList<SourceNode>();
	}

	public abstract void addEdge(SourceEdge edge);

	public abstract void addEdge(SourceNode fromNode, SourceNode toNode, int weight);

	public abstract void addNode(SourceNode node);

	public List<SourceEdge> getAllEdges() {
		return allEdges;
	}

	public List<SourceNode> getAllNodes() {
		return allNodes;
	}

	public abstract void removeEdge(SourceEdge edge);

	public abstract void removeNode(SourceNode node);

	public void setAllEdges(List<SourceEdge> allEdges) {
		this.allEdges = allEdges;
	}

	public void setAllNodes(List<SourceNode> allNodes) {
		this.allNodes = allNodes;
	}

	@Override
	public String toString() {
		return "SourceGraph [allEdges=" + allEdges + ", allNodes=" + allNodes + "]";
	}

	

}
