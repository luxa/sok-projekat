package rs.ac.uns.ftn.sok.host.model.graph.impl;

import rs.ac.uns.ftn.sok.host.model.graph.SourceEdge;
import rs.ac.uns.ftn.sok.host.model.graph.SourceNode;

public class SourceEdgeImpl extends SourceEdge {
	

	public SourceEdgeImpl() {
	}

	public SourceEdgeImpl(SourceNode fromNode, SourceNode toNode, int weight) {
		super(fromNode, toNode, weight);
	}

}
