# PROJEKAT IZ PREDMETA SOFTVERSKI OBRAZCI I KOMPONENTE
## ExPreSsiVeNess Tim7

###### Cilj projekta:
Implementirana aplikacija treba da bude plug-abilnog tipa, odnosno treba da podržava mogućnost uključivanja i isključivanja različitih plug-ina koji će raditi nezavisno jedan od drugog. U zavisnosti od vrste plug-ina treba da se omogući parsiranje različitih izvora, kao i vizualizacija i navigacija istih.

#### 1. Konfiguracija OSGi Framework-a
Nakon importovanja projekata u Eclipse desnim klikom miša na bilo koji od ponuđenih odabrati opciju Run as -> Run Configurations...
Dvoklikom na OSGi Framework otvara se New Configuration koji treba podesiti.
U kartici Bundles potrebno je obeležiti:
* Workspace - sve
* gogo - sve
* console - org.eclipse.equinox.console
VAŽNA NAPOMENA:
Prilikom pokretanja u RunConfigurationu -> Workspace kod ProjectHosta obavezno namestiti StartLevel na 1.

Nakon toga, klikom na dugme Add Required Bundles i Applay dodati su svi potrebni bundle-ovi i kreirana je nova konfiguracija.

#### 2. Pokretanje aplikacije
Prilikom pokretanja aplikacije moguće je da se javi Invalid Thread Access greška.
Ukoliko se to desi potrebno je:

```
    while( !pokrenuto){
		1) File -> Restart
		2) Project -> Clean...
		3) Refresh-ovati sve projekte
    }
    
```
i uživajte u našoj aplikaciji :blush:

#### 3. Korišćenje aplikacije
##### 3.1 Učitavanje izvora
Klikom na odgovarajuće dugme učitati jedan od ponuđenih izvora. JavaSource plug-in omogućava parsiranje samo i isključivo projekata pisanih u javi. DatabaseSource plug-in omogućava parsiranje već postojeće baze zadavanjem traženih parametara sa konkciju sa bazom.

##### 3.2 Pokretanje vizualizatora
Klikom na odgovarajuće dugme pokrenuti jedan od ponuđenih vizualizatora. PlainVisualiser plug-in omogućava iscrtavanje klasične strukture grafa, gde su čvorovi u zavisnosti od svoje težine obojeni različitom bojom. UMLVisualiser plug-in omogućava prikaz čvorova grafa prema UML specifikaciji.

#### 4. Dodatne funkcionalnosti
###### 4.1. Zoom in
Glavni canvas sa iscrtanim grafom se može uveličati akcijom CTRL+scroll ili klikom odgovarajućeg dugmeta iz toolbar-a. 

###### 4.2. Zoom out
Glavni canvas sa iscrtanim grafom se može umanjiti akcijom CTRL+scroll ili klikom odgovarajućeg dugmeta iz toolbar-a. 

###### 4.3. Pan
Glavni canvas sa iscrtanim grafom se može pomerati u željenom pravcu odabirom odgovarajućeg dugmeta iz toolbar-a.Vraćanje u default stanje (bez pan funkcionalnosti) moguće je klikom na odgovarajuću akciju u toolbar-u.   

###### 4.4. Search
Upisom odgovarajućeg teksta u tekst polje toolbar-a iscrtavaju se samo čvorovi koji odgovaraju zadatom filteru.

###### 4.5. Bird view
Bird view omogućava bolji pregled i lakšu navigaciju po iscrtanom grafu.

###### 4.6. Tree view
Tree view omogućava prikaz čvorova u strukturi stabla.

###### 4.7. Odabir težine
Odabirom akcije filter iz toolbar-a moguće je podesiti najveću težinu čvorova koji će se iscrtavati. Težina je srazmerna prioritetu čvora u grafu.

### Autori:
sw21-2013 Nikola Lukić
sw8-2013 Stefan Bratić
sw34-2013 Nina Simić
sw66-2014 Aleksandra Milivojević
