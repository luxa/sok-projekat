package rs.ac.uns.ftn.sok.uml;

import java.util.Hashtable;

import org.eclipse.swt.graphics.ImageData;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

import rs.ac.uns.ftn.sok.host.model.IVisualiser;
import rs.ac.uns.ftn.sok.uml.impl.UMLVisualisator;

public class Activator implements BundleActivator {

	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception {
		Thread.sleep(100);
		final Hashtable<String, Object> dict = new Hashtable<>();
		dict.put("VISUALISER", new UMLVisualisator());
		dict.put("NAME", "");
		dict.put("DESCRIPTION", "Uml vizualizator");
		ImageData data = new ImageData(getClass().getClassLoader().getResourceAsStream("icons/uml3.png"));
		dict.put("IMAGE", data);
		context.registerService(IVisualiser.class.getName(), new UMLVisualisator(), dict);
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		System.out.println("Goodbye World!!");
	}

}
