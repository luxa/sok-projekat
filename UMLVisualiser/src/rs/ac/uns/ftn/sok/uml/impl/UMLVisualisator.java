package rs.ac.uns.ftn.sok.uml.impl;

import java.util.HashMap;
import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseWheelListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.zest.core.widgets.Graph;
import org.eclipse.zest.core.widgets.GraphConnection;
import org.eclipse.zest.core.widgets.GraphItem;
import org.eclipse.zest.core.widgets.GraphNode;
import org.eclipse.zest.core.widgets.IContainer;
import org.eclipse.zest.core.widgets.LayoutFilter;
import org.eclipse.zest.core.widgets.ZestStyles;
import org.eclipse.zest.layouts.algorithms.SpringLayoutAlgorithm;
import rs.ac.uns.ftn.sok.host.model.IVisualiser;
import rs.ac.uns.ftn.sok.host.model.graph.SourceEdge;
import rs.ac.uns.ftn.sok.host.model.graph.SourceGraph;
import rs.ac.uns.ftn.sok.host.model.graph.SourceNode;

public class UMLVisualisator implements IVisualiser {

	private SourceGraph graph;
	private int weight;
	HashMap<Long, SourceNode> klase;
	HashMap<Long, SourceNode> atributi;
	HashMap<Long, SourceNode> metode;
	HashMap<Long, GraphNode> nacrtaneKlase;
	GraphConnection nasledjivanje;
	GraphConnection asocijacije;
	GraphConnection implementacija;
	public static Color classColor = new Color(null, 255, 255, 206);

	@Override
	public Graph drawGraph(SourceGraph graph, Composite panel, int weight) {
		this.graph = graph;
		this.weight = weight;

		panel.setLayout(new FillLayout());
		Font classFont = new Font(null, "Arial", 12, SWT.BOLD);
		Image classImage = new Image(Display.getDefault(), UMLClassFigure.class.getResourceAsStream("class_obj.gif"));
		Image privateField = new Image(Display.getDefault(),
				UMLClassFigure.class.getResourceAsStream("field_private_obj.gif"));
		Image publicField = new Image(Display.getDefault(),
				UMLClassFigure.class.getResourceAsStream("methpub_obj.gif"));

		Graph g = new Graph(panel, SWT.NONE);
		g.setConnectionStyle(ZestStyles.CONNECTIONS_DIRECTED);

		LayoutFilter filter = new LayoutFilter() {
			public boolean isObjectFiltered(GraphItem item) {
				if (item instanceof GraphConnection) {
					GraphConnection connection = (GraphConnection) item;
					Object data = connection.getData();
					return true;
				}
				return false;
			}
		};
		g.addLayoutFilter(filter);

		g.addMouseWheelListener(new MouseWheelListener() {

			@Override
			public void mouseScrolled(MouseEvent e) {
				if (e.stateMask == SWT.CTRL) {
					if (e.count > 0) {
						Point p = g.getZoomManager().getViewport().getViewLocation();
						Point p1 = new Point(e.x, e.y);
						p1.x += p.x;
						p1.y += p.y;
						Point p2 = p1.getCopy();
						double prev_zoom = g.getZoomManager().getZoom();
						double new_zoom = prev_zoom + 0.2;
						if (new_zoom < 4) {
							g.getZoomManager().setZoom(new_zoom);
							p2.scale(new_zoom / prev_zoom);
							Dimension dif = p2.getDifference(p1);
							p.x += dif.width;
							p.y += dif.height;
							g.getZoomManager().getViewport().setViewLocation(p);
						}

					} else {
						Point p = g.getZoomManager().getViewport().getViewLocation();
						Point p1 = new Point(e.x, e.y);
						p1.x += p.x;
						p1.y += p.y;
						Point p2 = p1.getCopy();
						double prev_zoom = g.getZoomManager().getZoom();
						double new_zoom = prev_zoom - 0.2;
						if (new_zoom > 0) {
							g.getZoomManager().setZoom(new_zoom);
							p2.scale(new_zoom / prev_zoom);
							Dimension dif = p2.getDifference(p1);
							p.x += dif.width;
							p.y += dif.height;
							g.getZoomManager().getViewport().setViewLocation(p);
						}
					}
				}
			}
		});

		final SpringLayoutAlgorithm springLayoutAlgorithm = new SpringLayoutAlgorithm();
		springLayoutAlgorithm.fitWithinBounds = false;
		springLayoutAlgorithm.setSpringMove(0.3);
		springLayoutAlgorithm.setResizing(false);
		g.setLayoutAlgorithm(springLayoutAlgorithm, true);
		klase = new HashMap<Long, SourceNode>();
		atributi = new HashMap<Long, SourceNode>();
		metode = new HashMap<Long, SourceNode>();

		for (SourceEdge e : graph.getAllEdges()) {

			klase.put(e.getFromNode().getId(), e.getFromNode());
			if (e.getWeight() <= 1) {
				klase.put(e.getToNode().getId(), e.getToNode());
			} else if (e.getWeight() == 2) {
				atributi.put(e.getToNode().getId(), e.getToNode());
			} else if (e.getWeight() == 3) {
				metode.put(e.getToNode().getId(), e.getToNode());
			}
		}

		nacrtaneKlase = new HashMap<>();
		for (SourceNode sn : klase.values()) {
			GraphNode n = new UMLNode(g, SWT.NONE,
					createClassFigure(sn, classFont, classColor, classImage, publicField, privateField));
			n.setText(sn.getName());
			nacrtaneKlase.put(sn.getId(), n);
		}

		for (SourceEdge e : graph.getAllEdges()) {
			if (e.getWeight() == 0) {
				nasledjivanje = new GraphConnection(g, SWT.NONE, nacrtaneKlase.get(e.getFromNode().getId()),
						nacrtaneKlase.get(e.getToNode().getId()));
				nasledjivanje.setLineColor(ColorConstants.black);
				nasledjivanje.setText("<<inheritance>>");
			} else if (e.getWeight() == 1) {
				asocijacije = new GraphConnection(g, SWT.NONE, nacrtaneKlase.get(e.getFromNode().getId()),
						nacrtaneKlase.get(e.getToNode().getId()));
				asocijacije.setLineColor(ColorConstants.gray);
			} else if (e.getWeight() == -1) {
				implementacija = new GraphConnection(g, SWT.NONE, nacrtaneKlase.get(e.getFromNode().getId()),
						nacrtaneKlase.get(e.getToNode().getId()));
				implementacija.setLineColor(ColorConstants.darkGreen);
				implementacija.setText("<<implementation>>");
			}

		}

		g.setLayoutAlgorithm(springLayoutAlgorithm, true);
		g.pack();

		return g;
	}

	public static IFigure createClassFigure1(Font classFont, Image classImage, Image publicField, Image privateField) {
		
		Label classLabel1 = new Label("Table", classImage);
		classLabel1.setFont(classFont);

		UMLClassFigure classFigure = new UMLClassFigure(classLabel1);
		Label attribute1 = new Label("columns: Column[]", privateField);
		Label attribute2 = new Label("rows: Row[]", privateField);
		Label method1 = new Label("getColumns(): Column[]", publicField);
		Label method2 = new Label("getRows(): Row[]", publicField);
		
		classFigure.getAttributesCompartment().add(attribute1);
		classFigure.getAttributesCompartment().add(attribute2);
		classFigure.getMethodsCompartment().add(method1);
		classFigure.getMethodsCompartment().add(method2);
		classFigure.setSize(-1, -1);

		return classFigure;
	}

	public IFigure createClassFigure(SourceNode sn, Font classFont, Color classColor, Image classImage,
			Image publicField, Image privateField) {
		Label classLabel2 = new Label(sn.getName(), classImage);
		classLabel2.setFont(classFont);
		UMLClassFigure classFigure = new UMLClassFigure(classLabel2);

		if (weight >= 2) {
			for (SourceEdge e : graph.getAllEdges()) {
				if (e.getWeight() == 2 && e.getFromNode().getId() == sn.getId()) {
					Label attribute = new Label(e.getToNode().getName(), privateField);
					classFigure.getAttributesCompartment().add(attribute);
				}
			}
		}

		if (weight >= 3) {
			for (SourceEdge e : graph.getAllEdges()) {
				if (e.getWeight() == 3 && e.getFromNode().getId() == sn.getId()) {
					Label attribute = new Label(e.getToNode().getName(), publicField);
					classFigure.getMethodsCompartment().add(attribute);
				}
			}
		}

		classFigure.setSize(-1, -1);

		return classFigure;
	}

	static class UMLNode extends GraphNode {

		IFigure customFigure = null;

		public UMLNode(IContainer graphModel, int style, IFigure figure) {
			super(graphModel, style, figure);
		}

		protected IFigure createFigureForModel() {
			return (IFigure) this.getData();
		}

	}

}
