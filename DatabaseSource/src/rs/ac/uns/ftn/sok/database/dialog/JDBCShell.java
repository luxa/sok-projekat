package rs.ac.uns.ftn.sok.database.dialog;

import java.sql.DriverManager;
import java.sql.SQLException;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import rs.ac.uns.ftn.sok.database.impl.DatabaseSource;

public class JDBCShell extends Shell {

	Text address;
	Text port;
	Text database;
	Text username;
	Text password;

	private DatabaseSource source;

	public JDBCShell(Shell parent, DatabaseSource source) {
		super(parent, SWT.APPLICATION_MODAL | SWT.SHEET);
		this.source = source;
		createJDBCDialog();
	}

	private void createJDBCDialog() {

		setLayout(new GridLayout(3, false));

		// row 1x
		final Label addressLabel = new Label(this, SWT.LEFT);
		addressLabel.setText("IP address:");
		addressLabel.pack();

		GridData data = new GridData(GridData.FILL, GridData.CENTER, true, true);
		data.horizontalSpan = 2;
		address = new Text(this, SWT.LEFT);
		address.setText("localhost");
		address.setLayoutData(data);
		address.pack();

		// row 2x
		final Label portLabel = new Label(this, SWT.LEFT);
		portLabel.setText("Port:");
		portLabel.pack();

		data = new GridData(GridData.FILL, GridData.CENTER, true, true);
		data.horizontalSpan = 2;
		port = new Text(this, SWT.LEFT);
		port.setText("3306");
		port.setLayoutData(data);
		port.pack();

		// row 3x
		final Label databaseLabel = new Label(this, SWT.LEFT);
		databaseLabel.setText("Database name:");
		databaseLabel.pack();

		data = new GridData(GridData.FILL, GridData.CENTER, true, true);
		data.horizontalSpan = 2;
		database = new Text(this, SWT.LEFT);
		database.setText("");
		database.setLayoutData(data);
		database.pack();

		// row 3
		final Label usernameLabel = new Label(this, SWT.LEFT);
		usernameLabel.setText("Username:");
		usernameLabel.pack();

		data = new GridData(GridData.FILL, GridData.CENTER, true, true);
		data.horizontalSpan = 2;
		username = new Text(this, SWT.LEFT);
		username.setLayoutData(data);
		username.setText("root");
		username.pack();
		// row 4

		final Label passwordLabel = new Label(this, SWT.LEFT);
		passwordLabel.setText("Password:");
		passwordLabel.pack();

		data = new GridData(GridData.FILL, GridData.CENTER, true, true);
		data.horizontalSpan = 2;
		password = new Text(this, SWT.LEFT | SWT.PASSWORD);
		password.setLayoutData(data);
		password.pack();

		final Button cancelBtn = new Button(this, SWT.PUSH);
		cancelBtn.setText("Cancel");
		data = new GridData(GridData.FILL, GridData.CENTER, true, true);
		cancelBtn.setLayoutData(data);
		cancelBtn.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				JDBCShell.this.close();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
				widgetSelected(arg0);
			}
		});

		Button saveBtn = new Button(this, SWT.PUSH);
		saveBtn.setText("Save");
		saveBtn.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				final String connectionString = String.format("jdbc:mysql://%s:%s/%s", address.getText(), port.getText(), database.getText());
				try {
					source.setConnection(DriverManager.getConnection(connectionString, username.getText(), password.getText()));
				} catch (SQLException e1) {
					e1.printStackTrace();
				}

				JDBCShell.this.close();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				widgetSelected(e);
			}
		});
		data = new GridData(GridData.FILL, GridData.CENTER, true, true);
		saveBtn.setLayoutData(data);
		setDefaultButton(saveBtn);
		setSize(600, 300);
	}

	@Override
	protected void checkSubclass() {
	}
}
