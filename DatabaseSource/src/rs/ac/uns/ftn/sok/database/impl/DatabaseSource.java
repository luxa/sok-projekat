package rs.ac.uns.ftn.sok.database.impl;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import rs.ac.uns.ftn.sok.database.dialog.JDBCShell;
import rs.ac.uns.ftn.sok.host.MainApplicationWindow;
import rs.ac.uns.ftn.sok.host.model.ISource;
import rs.ac.uns.ftn.sok.host.model.graph.SourceGraph;
import rs.ac.uns.ftn.sok.host.model.graph.SourceNode;
import rs.ac.uns.ftn.sok.host.model.graph.impl.SourceGraphImpl;
import rs.ac.uns.ftn.sok.host.model.graph.impl.SourceNodeImpl;

public class DatabaseSource implements ISource {
	
	private class Tuple {
		String first;
		String second;
		
		Tuple() {}

		public Tuple(String first, String second) {
			super();
			this.first = first;
			this.second = second;
		};
		
	}
	
	private HashMap<String, SourceNode> classNodes;
	private ArrayList<Tuple> tuples; 
	private MainApplicationWindow window;

	static {
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
		} catch (InstantiationException e1) {
			e1.printStackTrace();
		} catch (IllegalAccessException e1) {
			e1.printStackTrace();
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		}
	}
	private Connection connection;

	@Override
	public SourceGraph getSourceGraph(MainApplicationWindow window) {
		this.window = window;
		JDBCShell dialog = new JDBCShell(window.getShell(), this);
		dialog.open();
		
		return null;
	}

	public void setConnection(Connection connection) {
		this.connection = connection;
		SourceGraph g = new SourceGraphImpl();
		classNodes = new HashMap<>();
		tuples = new ArrayList<>();
		
		try {
			if (connection != null) {
				DatabaseMetaData metadata = connection.getMetaData();
				ResultSet tables = metadata.getTables(null, null, null, new String[] { "TABLE" });
				
				while (tables.next()) {
					final String tableName = tables.getString("TABLE_NAME");
					final SourceNode tableNode = new SourceNodeImpl(tableName);
					classNodes.put(tableName, tableNode);
					g.addNode(tableNode);
					
					System.out.println(tableName);
					final ResultSet columns = metadata.getColumns(null, null, tableName, null);
					while (columns.next()) {
						final String columnName = String.format("%s : %s", columns.getString("COLUMN_NAME"), columns.getString("TYPE_NAME"));
						final SourceNode columnNode = new SourceNodeImpl(columnName);
						g.addNode(columnNode);
						g.addEdge(tableNode, columnNode, 2);
						System.out.println(columnName);
					}
					System.out.println("\t==================================");
					final ResultSet veze = metadata.getImportedKeys(null, null, tableName);
					while (veze.next()) {
						System.out.println(String.format("\tPK : %-20s : %-20s : %-20s", veze.getString("PKTABLE_NAME"), veze.getString("FKCOLUMN_NAME"), veze.getString("PKCOLUMN_NAME")));
						final String pkTableName = veze.getString("PKTABLE_NAME");
						tuples.add(new Tuple(pkTableName, tableName));
						final SourceNode pkNode = new SourceNodeImpl(pkTableName);
//						classNodes.put(pkTableName, pkNode);
						g.addNode(pkNode);
					}
				}
				
				for (Tuple t : tuples) {
					System.out.printf("------ %s  %s --------", t.first, t.second);
					g.addEdge(classNodes.get(t.first), classNodes.get(t.second), 1);
				}

				connection.close();
				window.setSourceGraph(g);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
}
