package rs.ac.uns.ftn.sok.database;

import java.util.Hashtable;

import org.eclipse.swt.graphics.ImageData;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

import rs.ac.uns.ftn.sok.database.impl.DatabaseSource;
import rs.ac.uns.ftn.sok.host.model.ISource;

public class Activator implements BundleActivator {

	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception {
		Thread.sleep(100);
		Hashtable<String, Object> dict = new Hashtable<>();
		dict.put("NAME", "");
		dict.put("TOOLTIP", "Napravite graf od postojece baze podataka.");
		ImageData data = new ImageData(getClass().getClassLoader().getResourceAsStream("icons/database3.png"));
		dict.put("IMAGE", data);
		dict.put("SOURCE", new DatabaseSource());
		context.registerService(ISource.class.getName(), new DatabaseSource(), dict);
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		System.out.println("Goodbye World!!");
	}

}
