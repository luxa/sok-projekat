package rs.ac.uns.ftn.sok.javasource.model;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.core.dom.FieldDeclaration;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.TypeDeclaration;

import rs.ac.uns.ftn.sok.host.model.graph.SourceGraph;
import rs.ac.uns.ftn.sok.host.model.graph.SourceNode;
import rs.ac.uns.ftn.sok.host.model.graph.impl.SourceNodeImpl;

public class ClassModel {

	private final SourceNode classNode;
	private final List<FieldDeclaration> fieldDeclarations;
	private final List<MethodDeclaration> methodDeclarations;
	private TypeDeclaration typeDeclaration;

	public ClassModel(SourceNode classNode) {
		this.classNode = classNode;
		this.fieldDeclarations = new ArrayList<>();
		this.methodDeclarations = new ArrayList<>();
	}

	public void AddField(FieldDeclaration fd) {
		fieldDeclarations.add(fd);
	}

	public void AddMethod(MethodDeclaration md) {
		methodDeclarations.add(md);
	}

	public void constructChildNodes(SourceGraph resultGraph) {

		constructFields(resultGraph);
		constructMethods(resultGraph);
	}

	private void constructFields(SourceGraph resultGraph) {
		for (FieldDeclaration fd : fieldDeclarations) {
			final SourceNode fieldNode = new SourceNodeImpl();
			final StringBuilder fieldName = new StringBuilder();

			for (Object s : fd.modifiers()) {
				if (!s.toString().contains("@"))
					fieldName.append(s.toString() + " ");
			}

			fieldName.append(fd.getType() + " ");
			fieldName.append(extractFragment(fd.fragments().get(0).toString()));
			fieldNode.setName(fieldName.toString());
			System.out.println(fieldName.toString());
			resultGraph.addNode(fieldNode);
			resultGraph.addEdge(classNode, fieldNode, 2);

		}

	}

	private void constructMethods(SourceGraph resultGraph) {
		for (MethodDeclaration md : methodDeclarations) {
			final SourceNode methodNode = new SourceNodeImpl();
			final StringBuilder methodName = new StringBuilder();

			if (md.getName().toString().startsWith("get") || md.getName().toString().startsWith("set")) {
				return;
			}

			for (Object s : md.modifiers()) {

				if (!s.toString().contains("@")) {
					methodName.append(s.toString() + " ");
				}

			}

			if (!md.isConstructor()) {
				methodName.append(md.getReturnType2() + " ");
			}
			methodName.append(md.getName() + " ");
			methodName.append('(');
			for (Object s : md.parameters()) {
				String[] ss = s.toString().split(" ");
				String constructedParameter = "";
				for (String i : ss) {
					if (!i.contains("@")) {
						constructedParameter += i + " ";
					}
				}

				methodName.append(constructedParameter + ", ");
			}
			int lastIndexWs = methodName.lastIndexOf(", ");
			if (lastIndexWs != -1)
				methodName.deleteCharAt(lastIndexWs);

			methodName.append(')');

			methodNode.setName(methodName.toString().replaceAll("\\s+\\)", ")"));
			resultGraph.addNode(methodNode);
			resultGraph.addEdge(classNode, methodNode, 3);

		}
	}

	private String extractClassName(SourceNode node) {
		return node.getName().substring(node.getName().lastIndexOf('.') + 1, node.getName().length());
	}

	private String extractFragment(String fragment) {
		String[] retVal = fragment.split("=");

		if (retVal[0].contains("=")) {
			retVal[0] = retVal[0].substring(0, retVal[0].length() - 1);
		}

		return retVal[0];
	}

	public SourceNode getClassNode() {
		return classNode;
	}

	public List<FieldDeclaration> getFieldDeclaration() {
		return fieldDeclarations;
	}

	public List<FieldDeclaration> getFieldDeclarations() {
		return fieldDeclarations;
	}

	public List<MethodDeclaration> getMethodDeclaration() {
		return methodDeclarations;
	}

	public List<MethodDeclaration> getMethodDeclarations() {
		return methodDeclarations;
	}

	public TypeDeclaration getTypeDeclaration() {
		return typeDeclaration;
	}

	public boolean hasReference(SourceNode node) {
		for (FieldDeclaration fd : fieldDeclarations) {
			if (fd.getType().isArrayType() && fd.getType().toString().contains(extractClassName(node))) {
				System.out.println(fd.getType().toString() + " - " + node.getName());
				return true;
			}

			if (!fd.getType().isPrimitiveType() && fd.getType().toString().contains(extractClassName(node))) {
				System.out.println(fd.getType().toString() + " - " + node.getName());
				return true;
			}

		}

		return false;
	}

	public void setTypeDeclaration(TypeDeclaration typeDeclaration) {
		this.typeDeclaration = typeDeclaration;
	}

	public boolean extendsClass(SourceNode classNode2) {
		try {
			System.out.println("--------------------------------------------");
			System.out.println(classNode.getName());
			System.out.println(typeDeclaration.getName());
			System.out.println("isInterface-> " + typeDeclaration.isInterface());

			if (typeDeclaration.isInterface()) {
				return implementsInterface(classNode2);
			}

			if (typeDeclaration.getSuperclassType() == null)
				return false;
		} catch (Exception e) {
			return false;
		}

		return typeDeclaration.getSuperclassType().toString().equals(extractClassName(classNode2));

	}

	public boolean implementsInterface(SourceNode classNode2) {
		try {
			if (typeDeclaration.superInterfaceTypes().isEmpty() || typeDeclaration.isInterface())
				return false;

			for (Object t : typeDeclaration.superInterfaceTypes()) {
				System.out.println("implementsInterface " + t.toString());
				if (t.toString().equals(extractClassName(classNode2))) {
					return true;
				}
			}
		} catch (Exception e) {
			System.out.println("Uhvacen exception ClassModel.implementsinterface");
			return false;
		}

		return false;
	}

}
