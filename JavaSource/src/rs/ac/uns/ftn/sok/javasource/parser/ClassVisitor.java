package rs.ac.uns.ftn.sok.javasource.parser;

import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.FieldDeclaration;
import org.eclipse.jdt.core.dom.IBinding;
import org.eclipse.jdt.core.dom.MemberRef;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.TypeDeclaration;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;

import rs.ac.uns.ftn.sok.javasource.model.ClassModel;

public final class ClassVisitor extends ASTVisitor {

	private final ClassModel classModel;

	public ClassVisitor(ClassModel classModel) {
		this.classModel = classModel;

	}

	@Override
	public boolean visit(TypeDeclaration node) {
		classModel.setTypeDeclaration(node);
		return super.visit(node);
	}

	@Override
	public boolean visit(FieldDeclaration fd) {
		classModel.AddField(fd);
		return false;
	}

	@Override
	public boolean visit(MethodDeclaration md) {
		classModel.AddMethod(md);
		return false;
	}

}
