package rs.ac.uns.ftn.sok.javasource.parser;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.function.Consumer;

import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.swt.widgets.DirectoryDialog;

import rs.ac.uns.ftn.sok.host.MainApplicationWindow;
import rs.ac.uns.ftn.sok.host.model.ISource;
import rs.ac.uns.ftn.sok.host.model.graph.SourceGraph;
import rs.ac.uns.ftn.sok.host.model.graph.SourceNode;
import rs.ac.uns.ftn.sok.host.model.graph.impl.SourceGraphImpl;
import rs.ac.uns.ftn.sok.host.model.graph.impl.SourceNodeImpl;
import rs.ac.uns.ftn.sok.javasource.model.ClassModel;

public class JavaParser implements ISource {

	private static final String FILE_SUFFIX = "java";

	private HashMap<String, String> loadedFiles;
	private SourceGraph resultGraph;
	private List<ClassModel> foundedClasses;

	public JavaParser() {
		loadedFiles = new HashMap<String, String>();
		foundedClasses = new ArrayList<>();
	}

	private void uploadFiles(String projectPath) throws IOException {
		final File projectDirectory = new File(projectPath);
		if (projectDirectory.exists() && projectDirectory.isDirectory()) {
			File[] children = projectDirectory.listFiles();

			for (File file : children) {
				if (file.isDirectory()) {
					processDirectory(file);
				} else {
					processFile(file);
				}
			}

		} else {
			System.out.println("File doesn't exist or it is not a directory!");
		}
	}

	private void processDirectory(File directory) throws IOException {
		final File[] children = directory.listFiles();
		for (File child : children) {
			if (child.isDirectory()) {
				processDirectory(child);
			} else {
				processFile(child);
			}
		}
	}

	private void processFile(File file) throws IOException {
		if (getExtension(file) != null && getExtension(file).equals(FILE_SUFFIX)) {
			BufferedReader reader = new BufferedReader(new FileReader(file));

			String line;
			StringBuilder content = new StringBuilder();
			while ((line = reader.readLine()) != null) {
				content.append(line);
			}
			loadedFiles.put(file.getAbsolutePath(), content.toString());
			String className = file.getAbsolutePath().replaceFirst(".*src", "").replace(File.separator, ".")
					.substring(1);

			className = className.substring(0, className.lastIndexOf('.'));
			parseClass(className, content.toString());

		}
	}

	private String getExtension(File file) {
		if (file.isFile()) {
			final int i = file.getName().lastIndexOf('.');
			if (i > 0)
				return file.getName().substring(i + 1);
		}
		return null;
	}

	private void parseClass(String className, String content) {
		final ASTParser parser = ASTParser.newParser(AST.JLS8);
		parser.setSource(content.toCharArray());
		parser.setKind(ASTParser.K_COMPILATION_UNIT);

		final CompilationUnit cu = (CompilationUnit) parser.createAST(null);
		final SourceNode classNode = new SourceNodeImpl(className);
		resultGraph.addNode(classNode);

		ClassModel cm = new ClassModel(classNode);
		foundedClasses.add(cm);

		ClassVisitor cv = new ClassVisitor(cm);

		cu.accept(cv);

	}

	private void constructGraph() {
		for (ClassModel cm : foundedClasses) {
			for (ClassModel cm1 : foundedClasses) {
				if (cm.hasReference(cm1.getClassNode())) {
					resultGraph.addEdge(cm.getClassNode(), cm1.getClassNode(), 1);
				}
				if (cm.extendsClass(cm1.getClassNode())) {
					resultGraph.addEdge(cm.getClassNode(), cm1.getClassNode(), 0);
				}
				if (cm.implementsInterface(cm1.getClassNode())) {
					resultGraph.addEdge(cm.getClassNode(), cm1.getClassNode(), -1);
				}
			}
			cm.constructChildNodes(resultGraph);
		}
	}

	public void setSource(String source) {
		try {
			uploadFiles(source);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public SourceGraph getSourceGraph(MainApplicationWindow window) {
		loadedFiles = new HashMap<String, String>();
		foundedClasses = new ArrayList<>();

		resultGraph = new SourceGraphImpl();
		DirectoryDialog dlg = new DirectoryDialog(window.getShell());

		dlg.setText("Java Source");

		dlg.setMessage("Select root directory of a project.");

		String dir = dlg.open();
		if (dir != null) {
			setSource(dir);
		} else {
			return null;
		}

		constructGraph();
		return resultGraph;
	}

}
