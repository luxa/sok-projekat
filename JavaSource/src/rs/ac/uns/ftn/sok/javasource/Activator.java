package rs.ac.uns.ftn.sok.javasource;

import java.util.Hashtable;

import org.eclipse.swt.graphics.ImageData;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

import rs.ac.uns.ftn.sok.host.model.ISource;
import rs.ac.uns.ftn.sok.javasource.parser.JavaParser;

public class Activator implements BundleActivator {

	public void start(BundleContext context) throws Exception {
		Thread.sleep(100);
		Hashtable<String, Object> dict = new Hashtable<>();
		dict.put("NAME", "");
		dict.put("TOOLTIP", "Napravite graf od nekog java projekta.");
		ImageData data = new ImageData(getClass().getClassLoader().getResourceAsStream("icons/java3.png"));
		dict.put("IMAGE", data);
		dict.put("SOURCE", new JavaParser());
		context.registerService(ISource.class.getName(), new JavaParser(), dict);
	}
	
	public void stop(BundleContext context) throws Exception {
		System.out.println("Goodbye World!!");
	}

}
